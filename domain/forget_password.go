package domain

import (
	"context"
)

type ForgetPasswordRequest struct {
	Email string `from:"email" binding:"required,email"`
}

type ForgetPasswordOtpRequest struct {
	Email string `from:"email" binding:"required,email"`
	Otp   string `from:"otp" binding:"required"`
}

type ForgetPasswordOtpNewPasswordRequest struct {
	Email    string `from:"email" binding:"required,email"`
	Otp      string `from:"otp" binding:"required"`
	Password string `from:"password" binding:"required"`
}

type ForgetPasswordUsecase interface {
	GetUserByEmail(c context.Context, email string) (User, error)
	SaveUserOtp(c context.Context, user User, otp string) error
	SetNewPassword(c context.Context, user User, newpassword string) error
}

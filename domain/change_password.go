package domain

import "context"

type ChangePasswordRequest struct {
	Password    string `from:"password" binding:"required"`
	NewPassword string `from:"newPassword" binding:"required"`
}

type ChangePasswordUsecase interface {
	GetUserByID(c context.Context, id string) (User, error)
	SetNewPassword(c context.Context, user User, newpassword string) error
}

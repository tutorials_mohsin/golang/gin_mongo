package usecase

import (
	"context"
	"go_gin_mongo/domain"
	"time"
)

type forgetPasswordUsecase struct {
	userRepository domain.UserRepository
	contextTimeout time.Duration
}

func NewForgetPasswordUsecase(userRepositort domain.UserRepository, timeout time.Duration) domain.ForgetPasswordUsecase {
	return &forgetPasswordUsecase{
		userRepository: userRepositort,
		contextTimeout: timeout,
	}
}

func (fpu *forgetPasswordUsecase) GetUserByEmail(c context.Context, email string) (domain.User, error) {
	ctx, cancle := context.WithTimeout(c, fpu.contextTimeout)
	defer cancle()
	return fpu.userRepository.GetByEmail(ctx, email)
}

func (fpu *forgetPasswordUsecase) SaveUserOtp(c context.Context, user domain.User, otp string) error {
	ctx, cancle := context.WithTimeout(c, fpu.contextTimeout)
	defer cancle()
	return fpu.userRepository.SetUserOtp(ctx, user, otp)
}

func (fpu *forgetPasswordUsecase) SetNewPassword(c context.Context, user domain.User, newpassword string) error {
	ctx, cancle := context.WithTimeout(c, fpu.contextTimeout)
	defer cancle()
	return fpu.userRepository.SetNewPassword(ctx, user, newpassword)
}

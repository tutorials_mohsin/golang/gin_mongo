package usecase

import (
	"context"
	"go_gin_mongo/domain"
	"time"
)

type profileUsecase struct {
	userRepository domain.UserRepository
	contextTimeout time.Duration
}

func NewProfileUsecase(userRepository domain.UserRepository, timeout time.Duration) domain.ProfileUsecase {
	return &profileUsecase{
		userRepository: userRepository,
		contextTimeout: timeout,
	}

}

func (pu *profileUsecase) GetProfileByID(c context.Context, userID string) (*domain.Profile, error) {
	ctx, cancle := context.WithTimeout(c, pu.contextTimeout)
	defer cancle()
	user, err := pu.userRepository.GetByID(ctx, userID)
	if err != nil {
		return nil, err
	}
	return &domain.Profile{Name: user.Name, Email: user.Email}, nil
}

package usecase

import (
	"context"
	"go_gin_mongo/domain"
	"time"
)

type changePasswordUsecase struct {
	userRepository domain.UserRepository
	contextTimeout time.Duration
}

func NewChangePasswordUsecase(userRepositort domain.UserRepository, timeout time.Duration) domain.ChangePasswordUsecase {
	return &changePasswordUsecase{
		userRepository: userRepositort,
		contextTimeout: timeout,
	}
}

func (cpu *changePasswordUsecase) GetUserByID(c context.Context, id string) (domain.User, error) {
	ctx, cancle := context.WithTimeout(c, cpu.contextTimeout)
	defer cancle()
	return cpu.userRepository.GetByID(ctx, id)
}

func (cpu *changePasswordUsecase) SetNewPassword(c context.Context, user domain.User, newpassword string) error {
	ctx, cancle := context.WithTimeout(c, cpu.contextTimeout)
	defer cancle()
	return cpu.userRepository.SetNewPassword(ctx, user, newpassword)
}

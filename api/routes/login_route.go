package routes

import (
	"go_gin_mongo/api/controllers"
	"go_gin_mongo/config"
	"go_gin_mongo/domain"
	"go_gin_mongo/repository"
	"go_gin_mongo/usecase"

	"github.com/gin-gonic/gin"
)

func NewLoginRouter(app config.GoAppTools, group *gin.RouterGroup) {
	ur := repository.NewUserRepository(*app.Mongo, domain.CollectionUser)
	lc := controllers.LoginController{
		LoginUsecase: usecase.NewLoginUsecase(ur, app.ContextTimeout),
		App:          app,
	}
	group.POST("/login", lc.Login)
}

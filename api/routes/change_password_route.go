package routes

import (
	"go_gin_mongo/api/controllers"
	"go_gin_mongo/config"
	"go_gin_mongo/domain"
	"go_gin_mongo/repository"
	"go_gin_mongo/usecase"

	"github.com/gin-gonic/gin"
)

func NewChangePasswordRouter(app config.GoAppTools, group *gin.RouterGroup) {
	ur := repository.NewUserRepository(*app.Mongo, domain.CollectionUser)
	cpc := &controllers.ChangePasswordController{
		ChangePasswordUsecase: usecase.NewChangePasswordUsecase(ur, app.ContextTimeout),
	}

	group.POST("/changepassword", cpc.ChangePassword)
}

package routes

import (
	"go_gin_mongo/api/controllers"
	"go_gin_mongo/config"
	"go_gin_mongo/domain"
	"go_gin_mongo/repository"
	"go_gin_mongo/usecase"

	"github.com/gin-gonic/gin"
)

func NewForgetPasswordRouter(app config.GoAppTools, group *gin.RouterGroup) {
	ur := repository.NewUserRepository(*app.Mongo, domain.CollectionUser)
	fpc := &controllers.ForgetPasswordController{
		ForgetPasswordUsecase: usecase.NewForgetPasswordUsecase(ur, app.ContextTimeout),
		App:                   app,
	}

	group.POST("/forgetpassword", fpc.ForgetPassword)
	group.POST("/forgetpasswordotp", fpc.ForgetPasswordOtp)
	group.POST("/forgetpasswordotpnewpassword", fpc.ForgetPasswordOtpNewPassword)
}

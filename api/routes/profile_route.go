package routes

import (
	"go_gin_mongo/api/controllers"
	"go_gin_mongo/config"
	"go_gin_mongo/domain"
	"go_gin_mongo/repository"
	"go_gin_mongo/usecase"

	"github.com/gin-gonic/gin"
)

func NewProfileRouter(app config.GoAppTools, group *gin.RouterGroup) {
	ur := repository.NewUserRepository(*app.Mongo, domain.CollectionUser)
	pc := &controllers.ProfileController{
		ProfileUsecase: usecase.NewProfileUsecase(ur, app.ContextTimeout),
		App:            app,
	}
	group.GET("/profile", pc.Fetch)
}

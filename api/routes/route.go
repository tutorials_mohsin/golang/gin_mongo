package routes

import (
	"go_gin_mongo/api/middlewares"
	"go_gin_mongo/config"
)

func Setup(app config.GoAppTools) {

	publicRouter := app.Server.Group("/api")
	NewSignupRouter(app, publicRouter)
	NewLoginRouter(app, publicRouter)
	NewRefreshTokenRouter(app, publicRouter)
	NewForgetPasswordRouter(app, publicRouter)

	procetedRouter := app.Server.Group("/api")

	procetedRouter.Use(middlewares.JwtAuthMiddleware(app.AppConfig.AccessTokenPrivateKey))

	NewProfileRouter(app, procetedRouter)
	NewChangePasswordRouter(app, procetedRouter)

}

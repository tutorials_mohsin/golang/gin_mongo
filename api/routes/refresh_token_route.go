package routes

import (
	"go_gin_mongo/api/controllers"
	"go_gin_mongo/config"
	"go_gin_mongo/domain"
	"go_gin_mongo/repository"
	"go_gin_mongo/usecase"

	"github.com/gin-gonic/gin"
)

func NewRefreshTokenRouter(app config.GoAppTools, group *gin.RouterGroup) {
	ur := repository.NewUserRepository(*app.Mongo, domain.CollectionUser)
	rtc := &controllers.RefreshTokenController{
		RefreshTokenUsecase: usecase.NewRefreshTokenUsecase(ur, app.ContextTimeout),
		App:                 app,
	}
	group.POST("refresh", rtc.RefreshToken)
}

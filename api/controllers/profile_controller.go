package controllers

import (
	"go_gin_mongo/config"
	"go_gin_mongo/domain"
	"net/http"

	"github.com/gin-gonic/gin"
)

type ProfileController struct {
	ProfileUsecase domain.ProfileUsecase
	App            config.GoAppTools
}

// Fetch retrieves the profile information for the authenticated user.
// @Summary Fetch user profile
// @Description Retrieves the profile information for the authenticated user.
// @Accept json
// @Produce json
// @Tags Profile
// @Security BearerAuth
// @Success 200 {object} domain.Profile "Successful profile fetch"
// @Failure 500 {object} domain.ErrorResponse "Internal Server Error"
// @Router /api/profile [get]
func (pc *ProfileController) Fetch(c *gin.Context) {
	userId := c.GetString("x-user-id")
	profile, err := pc.ProfileUsecase.GetProfileByID(c, userId)
	if err != nil {
		pc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	pc.App.InfoLogger.Println(profile)
	c.JSON(http.StatusOK, profile)
}

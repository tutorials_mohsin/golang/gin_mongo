package controllers

import (
	"go_gin_mongo/config"
	"go_gin_mongo/domain"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type LoginController struct {
	LoginUsecase domain.LoginUsecase
	App          config.GoAppTools
}

// Login handles user login by validating credentials and generating access and refresh tokens.
// @Summary Authenticate user and generate tokens
// @Description Authenticates the user with the provided credentials and returns access and refresh tokens.
// @ID login
// @Accept json
// @Produce json
// @Tags Authentication
// @Param request body domain.LoginRequest true "User credentials for login"
// @Success 200 {object} domain.LoginResponse "Successful login"
// @Failure 400 {object} domain.ErrorResponse "Bad request"
// @Failure 401 {object} domain.ErrorResponse "Invalid credentials"
// @Failure 404 {object} domain.ErrorResponse "User not found"
// @Failure 500 {object} domain.ErrorResponse "Internal Server Error"
// @Router /api/login [post]
func (lc *LoginController) Login(c *gin.Context) {
	var request domain.LoginRequest

	err := c.ShouldBind(&request)
	if err != nil {
		lc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusBadRequest, domain.ErrorResponse{Message: err.Error()})
		return
	}

	user, err := lc.LoginUsecase.GetUserByEmail(c, request.Email)
	if err != nil {
		lc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusNotFound, domain.ErrorResponse{Message: "User not found with the given email"})
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(request.Password))
	if err != nil {
		lc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusUnauthorized, domain.ErrorResponse{Message: "Invalid credentials"})
		return
	}

	accessToken, err := lc.LoginUsecase.CreateAccessToken(&user, lc.App.AppConfig.AccessTokenPrivateKey, lc.App.AppConfig.AccessTokenMaxAge)
	if err != nil {
		lc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	refreshToken, err := lc.LoginUsecase.CreateRefreshToken(&user, lc.App.AppConfig.RefreshTokenPrivateKey, lc.App.AppConfig.RefreshTokenMaxAge)
	if err != nil {
		lc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	loginResponse := domain.LoginResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}

	lc.App.InfoLogger.Println(loginResponse)
	c.JSON(http.StatusOK, loginResponse)
}

package controllers

import (
	"go_gin_mongo/config"
	"go_gin_mongo/domain"
	"net/http"

	"github.com/gin-gonic/gin"
)

type RefreshTokenController struct {
	RefreshTokenUsecase domain.RefreshTokenUsecase
	App                 config.GoAppTools
}

// RefreshToken refreshes the access token using a valid refresh token.
// @Summary Refresh access token
// @Description Refreshes the access token using a valid refresh token.
// @Accept json
// @Produce json
// @Tags Authentication
// @Param refresh_token body domain.RefreshTokenRequest true "Refresh token for refreshing access token"
// @Success 200 {object} domain.RefreshTokenResponse "Successful token refresh"
// @Failure 400 {object} domain.ErrorResponse "Bad request"
// @Failure 401 {object} domain.ErrorResponse "Invalid refresh token"
// @Failure 500 {object} domain.ErrorResponse "Internal Server Error"
// @Router /api/refresh [post]
func (rtc *RefreshTokenController) RefreshToken(c *gin.Context) {
	var request domain.RefreshTokenRequest

	err := c.ShouldBind(&request)
	if err != nil {
		rtc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusBadRequest, domain.ErrorResponse{Message: err.Error()})
		return
	}

	id, err := rtc.RefreshTokenUsecase.ExtractIDFromToken(request.RefreshToken, rtc.App.AppConfig.RefreshTokenPrivateKey)
	if err != nil {
		rtc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusUnauthorized, domain.ErrorResponse{Message: "User not found"})
		return
	}

	user, err := rtc.RefreshTokenUsecase.GetUserByID(c, id)
	if err != nil {
		rtc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusUnauthorized, domain.ErrorResponse{Message: "User not found"})
		return
	}

	accessToken, err := rtc.RefreshTokenUsecase.CreateAccessToken(&user, rtc.App.AppConfig.AccessTokenPrivateKey, rtc.App.AppConfig.AccessTokenMaxAge)
	if err != nil {
		rtc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	refreshToken, err := rtc.RefreshTokenUsecase.CreateRefreshToken(&user, rtc.App.AppConfig.RefreshTokenPrivateKey, rtc.App.AppConfig.RefreshTokenMaxAge)
	if err != nil {
		rtc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	refreshTokenResponse := domain.RefreshTokenResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}

	rtc.App.InfoLogger.Println(refreshTokenResponse)
	c.JSON(http.StatusOK, refreshTokenResponse)

}

package controllers

import (
	"go_gin_mongo/config"
	"go_gin_mongo/domain"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

type SignupController struct {
	SignupUsecase domain.SignupUsecase
	App           config.GoAppTools
}

// @Summary Signup user
// @Description User signup with name, email, password API.
// @ID signup
// @Tags Authentication
// @Accept  json
// @Produce  json
// @Param   signupRequest     body    domain.SignupRequest     true        "Signup request parameters"
// @Success 200 {object} domain.SignupResponse "Successful signup response"
// @Failure 400 {object} domain.ErrorResponse "Bad request"
// @Failure 409 {object} domain.ErrorResponse "User already exists"
// @Failure 500 {object} domain.ErrorResponse "Internal server error"
// @Router /api/signup [post]
func (sc *SignupController) Signup(c *gin.Context) {
	var request domain.SignupRequest

	err := c.ShouldBind(&request)
	if err != nil {
		sc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusBadRequest, domain.ErrorResponse{Message: err.Error()})
		return
	}
	_, err = sc.SignupUsecase.GetUserByEmail(c, request.Email)
	if err == nil {
		sc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusConflict, domain.ErrorResponse{Message: "User already exists with the given email"})
		return
	}

	encryptedPassword, err := bcrypt.GenerateFromPassword(
		[]byte(request.Password),
		bcrypt.DefaultCost,
	)

	if err != nil {
		sc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	request.Password = string(encryptedPassword)

	user := domain.User{
		ID:       primitive.NewObjectID(),
		Name:     request.Name,
		Email:    request.Email,
		Password: request.Password,
	}
	err = sc.SignupUsecase.Create(c, &user)
	if err != nil {
		sc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	accessToken, err := sc.SignupUsecase.CreateAccessToken(&user, sc.App.AppConfig.AccessTokenPrivateKey, sc.App.AppConfig.AccessTokenMaxAge)
	if err != nil {
		sc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	refreshToken, err := sc.SignupUsecase.CreateRefreshToken(&user, sc.App.AppConfig.RefreshTokenPrivateKey, sc.App.AppConfig.RefreshTokenMaxAge)

	if err != nil {
		sc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	signupResponse := domain.SignupResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}

	sc.App.InfoLogger.Println(signupResponse)
	c.JSON(http.StatusOK, signupResponse)
}

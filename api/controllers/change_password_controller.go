package controllers

import (
	"go_gin_mongo/config"
	"go_gin_mongo/domain"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type ChangePasswordController struct {
	ChangePasswordUsecase domain.ChangePasswordUsecase
	App                   config.GoAppTools
}

// ChangePassword changes the password for the authenticated user.
// @Summary Change user password
// @Description Changes the password for the authenticated user.
// @Accept json
// @Produce json
// @Tags Authentication
// @Security BearerAuth
// @Param   changepasswordrequest     body    domain.ChangePasswordRequest     true        "Change Password request parameters"
// @Success 200 {object} domain.SuccessResponse "Password updated successfully"
// @Failure 400 {object} domain.ErrorResponse "Bad request"
// @Failure 401 {object} domain.ErrorResponse "Invalid credentials"
// @Failure 500 {object} domain.ErrorResponse "Internal Server Error"
// @Router /api/changepassword [post]
func (cpc *ChangePasswordController) ChangePassword(c *gin.Context) {
	var changePasswordRequest domain.ChangePasswordRequest

	err := c.ShouldBind(&changePasswordRequest)
	if err != nil {
		cpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusBadRequest, domain.ErrorResponse{Message: err.Error()})
		return
	}

	userID := c.GetString("x-user-id")

	user, err := cpc.ChangePasswordUsecase.GetUserByID(c, userID)
	if err != nil {
		cpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusUnauthorized, domain.ErrorResponse{Message: "User not found"})
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(changePasswordRequest.Password))
	if err != nil {
		cpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusUnauthorized, domain.ErrorResponse{Message: "Invalid credentials"})
		return
	}

	encryptedPassword, err := bcrypt.GenerateFromPassword(
		[]byte(changePasswordRequest.NewPassword),
		bcrypt.DefaultCost,
	)

	if err != nil {
		cpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	err = cpc.ChangePasswordUsecase.SetNewPassword(c, user, string(encryptedPassword))

	if err != nil {
		cpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{
			Message: "Internal server error",
		})
	}

	cpc.App.InfoLogger.Println("Password updated successfully")
	c.JSON(http.StatusOK, domain.SuccessResponse{
		Message: "Password updated successfully",
	})
}

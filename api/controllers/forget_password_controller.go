package controllers

import (
	"go_gin_mongo/config"
	"go_gin_mongo/domain"
	"go_gin_mongo/internal/emailutil"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/nanorand/nanorand"
	"golang.org/x/crypto/bcrypt"
)

type ForgetPasswordController struct {
	ForgetPasswordUsecase domain.ForgetPasswordUsecase
	App                   config.GoAppTools
}

// ForgetPassword initiates the forget password process for a user.
// @Summary Forget password
// @Description Initiates the forget password process for a user. Generates and saves an OTP.
// @Accept json
// @Produce json
// @Tags Authentication
// @Param   forgetpasswordrequest     body    domain.ForgetPasswordRequest     true        "Forget Password request parameters"
// @Success 200 {object} domain.SuccessResponse "An email sent"
// @Failure 400 {object} domain.ErrorResponse "Bad request"
// @Failure 404 {object} domain.ErrorResponse "User not found with the given email"
// @Failure 500 {object} domain.ErrorResponse "Internal Server Error"
// @Router /api/forgetpassword [post]
func (fpc *ForgetPasswordController) ForgetPassword(c *gin.Context) {
	var forgetPasswordRequest domain.ForgetPasswordRequest

	err := c.ShouldBind(&forgetPasswordRequest)
	if err != nil {
		fpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusBadRequest, domain.ErrorResponse{Message: err.Error()})
		return
	}

	user, err := fpc.ForgetPasswordUsecase.GetUserByEmail(c, forgetPasswordRequest.Email)
	if err != nil {
		fpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusNotFound, domain.ErrorResponse{Message: "User not found with the given email"})
		return
	}

	otp, err := nanorand.Gen(6)
	if err != nil {
		fpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	err = fpc.ForgetPasswordUsecase.SaveUserOtp(c, user, otp)
	if err != nil {
		fpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	go emailutil.SendOtpMail(*fpc.App.AppConfig, user, &emailutil.OtpMailData{Code: otp})

	fpc.App.InfoLogger.Println("An email sent")
	c.JSON(http.StatusOK, domain.SuccessResponse{
		Message: "An email sent",
	})
}

// ForgetPassword otp verivication process for a user.
// @Summary Forget password otp verify
// @Description Verifies otp of the forget password process for a user.
// @Accept json
// @Produce json
// @Tags Authentication
// @Param   forgetpasswordotprequest     body    domain.ForgetPasswordOtpRequest     true        "Forget Password Otp request parameters"
// @Success 200 {object} domain.SuccessResponse "An email sent"
// @Failure 400 {object} domain.ErrorResponse "Bad request"
// @Failure 404 {object} domain.ErrorResponse "User not found with the given email"
// @Failure 500 {object} domain.ErrorResponse "Internal Server Error"
// @Router /api/forgetpasswordotp [post]
func (fpc *ForgetPasswordController) ForgetPasswordOtp(c *gin.Context) {
	var forgetPasswordOtpRequest domain.ForgetPasswordOtpRequest

	err := c.ShouldBind(&forgetPasswordOtpRequest)
	if err != nil {
		fpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusBadRequest, domain.ErrorResponse{Message: err.Error()})
		return
	}

	user, err := fpc.ForgetPasswordUsecase.GetUserByEmail(c, forgetPasswordOtpRequest.Email)
	if err != nil {
		fpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusNotFound, domain.ErrorResponse{Message: "User not found with the given email"})
		return
	}

	if forgetPasswordOtpRequest.Otp != user.Otp {
		fpc.App.ErrorLogger.Println("Otp not matched")
		c.JSON(http.StatusUnauthorized, domain.ErrorResponse{Message: "Invalid OTP"})
		return
	}

	fpc.App.InfoLogger.Println("Otp verified")
	c.JSON(http.StatusOK, domain.SuccessResponse{
		Message: "Otp verified",
	})
}

// ForgetPassword initiates the forget password process for a user.
// @Summary Forget password otp set new password
// @Description Initiates the forget password process for a user. Generates and saves an OTP.
// @Accept json
// @Produce json
// @Tags Authentication
// @Param   forgetpasswordotpnewpasswordrequest     body    domain.ForgetPasswordOtpNewPasswordRequest     true        "Forget Password Otp NewPassword request parameters"
// @Success 200 {object} domain.SuccessResponse "An email sent"
// @Failure 400 {object} domain.ErrorResponse "Bad request"
// @Failure 404 {object} domain.ErrorResponse "User not found with the given email"
// @Failure 500 {object} domain.ErrorResponse "Internal Server Error"
// @Router /api/forgetpasswordotpnewpassword [post]
func (fpc *ForgetPasswordController) ForgetPasswordOtpNewPassword(c *gin.Context) {
	var forgetPasswordOtpNewPasswordRequest domain.ForgetPasswordOtpNewPasswordRequest

	err := c.ShouldBind(&forgetPasswordOtpNewPasswordRequest)
	if err != nil {
		fpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusBadRequest, domain.ErrorResponse{Message: err.Error()})
		return
	}

	user, err := fpc.ForgetPasswordUsecase.GetUserByEmail(c, forgetPasswordOtpNewPasswordRequest.Email)
	if err != nil {
		fpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusNotFound, domain.ErrorResponse{Message: "User not found with the given email"})
		return
	}

	if forgetPasswordOtpNewPasswordRequest.Otp != user.Otp {
		fpc.App.ErrorLogger.Println("Otp not matched")
		c.JSON(http.StatusUnauthorized, domain.ErrorResponse{Message: "Invalid OTP"})
		return
	}

	encryptedPassword, err := bcrypt.GenerateFromPassword(
		[]byte(forgetPasswordOtpNewPasswordRequest.Password),
		bcrypt.DefaultCost,
	)

	if err != nil {
		fpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	err = fpc.ForgetPasswordUsecase.SetNewPassword(c, user, string(encryptedPassword))

	if err != nil {
		fpc.App.ErrorLogger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, domain.ErrorResponse{Message: err.Error()})
		return
	}

	fpc.App.InfoLogger.Println("Password updated successfully")
	c.JSON(http.StatusOK, domain.SuccessResponse{
		Message: "Password updated sccessfully",
	})
}

package middlewares

import (
	"go_gin_mongo/domain"
	"go_gin_mongo/internal/tokenutil"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func JwtAuthMiddleware(secret string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authHeader := ctx.Request.Header.Get("Authorization")
		t := strings.Split(authHeader, " ")
		if len(t) == 2 {
			authToken := t[1]
			authorized, err := tokenutil.IsAuthorized(authToken, secret)
			if err != nil {
				ctx.JSON(http.StatusUnauthorized, domain.ErrorResponse{Message: err.Error()})
				ctx.Abort()
				return
			}
			if authorized {
				userID, err := tokenutil.ExtractIDFromToken(authToken, secret)
				if err != nil {
					ctx.JSON(http.StatusUnauthorized, domain.ErrorResponse{Message: err.Error()})
					ctx.Abort()
					return
				}
				ctx.Set("x-user-id", userID)
				ctx.Next()
				return
			}

			ctx.JSON(http.StatusUnauthorized, domain.ErrorResponse{Message: err.Error()})
			ctx.Abort()
			return

		}

		ctx.JSON(http.StatusUnauthorized, domain.ErrorResponse{Message: "Not authorized"})
		ctx.Abort()
	}
}

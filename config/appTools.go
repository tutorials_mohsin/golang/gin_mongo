package config

import (
	"log"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"go.mongodb.org/mongo-driver/mongo"
)

type GoAppTools struct {
	ErrorLogger    *log.Logger
	InfoLogger     *log.Logger
	Validate       *validator.Validate
	Mongo          *mongo.Database
	ContextTimeout time.Duration
	Server         *gin.Engine
	AppConfig      *AppConfig
}

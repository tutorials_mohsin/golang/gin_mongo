package emailutil

import (
	"bytes"
	"crypto/tls"
	"go_gin_mongo/config"
	"go_gin_mongo/domain"
	"log"

	"github.com/k3a/html2text"
	"gopkg.in/gomail.v2"
)

type OtpMailData struct {
	Code string
}

func SendOtpMail(appConfig config.AppConfig, user domain.User, data *OtpMailData) {
	from := appConfig.EmailFrom
	smtpPass := appConfig.SMTPPass
	smtpUser := appConfig.SMTPUser
	to := user.Email
	smtpHost := appConfig.SMTPHost
	smtpPort := appConfig.SMTPPort

	var body bytes.Buffer

	template, err := ParseTemplateDir("templates")
	if err != nil {
		log.Fatal("Could not parse template", err)
	}

	template.ExecuteTemplate(&body, "otpCode.html", &data)

	m := gomail.NewMessage()

	m.SetHeader("From", from)
	m.SetHeader("To", to)
	m.SetHeader("Subject", "Otp mail")
	m.SetBody("text/html", body.String())
	m.AddAlternative("text/plain", html2text.HTML2Text(body.String()))

	d := gomail.NewDialer(smtpHost, smtpPort, smtpUser, smtpPass)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// Send Email
	if err := d.DialAndSend(m); err != nil {
		log.Fatal("Could not send email: ", err)
	}
}

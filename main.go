package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"go_gin_mongo/api/routes"
	"go_gin_mongo/config"
	_ "go_gin_mongo/docs"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

var (
	app config.GoAppTools
)

func init() {
	appConfig, err := config.LoadConfig(".")
	if err != nil {
		log.Fatal("? Could not load environment variables", err)
	}

	app.AppConfig = appConfig

	InfoLogger := log.New(os.Stdout, "Info-> ", log.LstdFlags|log.Lshortfile)
	ErrorLogger := log.New(os.Stdout, "Error-> ", log.LstdFlags|log.Lshortfile)

	app.InfoLogger = InfoLogger
	app.ErrorLogger = ErrorLogger

	client := config.NewMongoDatabase(app.AppConfig)
	database := client.Database("gogin")
	app.Mongo = database

	timeout := time.Duration(appConfig.ContextTimeout) * time.Second
	app.ContextTimeout = timeout

	server := gin.Default()
	app.Server = server

	app.Server.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}

// @title Basic APIs
// @version 1.0
// @description Testing Swagger APIs.
// @securityDefinitions.apikey BearerAuth
// @in head
// @name Authorization
// @host localhost:8000
// @BasePath /api
// @schemes http, https
func main() {

	config, err := config.LoadConfig(".")
	if err != nil {
		log.Fatal("? Could not load environment variables", err)
	}

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowOrigins = []string{"http://localhost:8001", config.ClientOrigin}
	corsConfig.AllowCredentials = true

	app.Server.Use(cors.New(corsConfig))

	routes.Setup(app)

	router := app.Server.Group("/api")

	router.GET("/healthchecker", HealthCheck)

	log.Fatal(app.Server.Run(":" + config.ServerPort))

}

type HealthCheckResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// @Summary Health Check
// @Description Check the health of the API.
// @ID health-check
// @Produce json
// @Success 200 {object} HealthCheckResponse "Welcome message"
// @Router /api/healthchecker [get]
func HealthCheck(ctx *gin.Context) {
	response := HealthCheckResponse{
		Status:  "success",
		Message: "Welcome to Golang with Gorm and Postgres",
	}
	ctx.JSON(http.StatusOK, response)
}

package repository

import (
	"context"
	"go_gin_mongo/domain"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type userRepository struct {
	database   mongo.Database
	collection string
}

func NewUserRepository(database mongo.Database, collection string) domain.UserRepository {
	return &userRepository{
		database:   database,
		collection: collection,
	}
}

func (ur *userRepository) Create(c context.Context, user *domain.User) error {
	collection := ur.database.Collection(ur.collection)
	_, err := collection.InsertOne(c, user)
	return err
}

func (ur *userRepository) GetByEmail(c context.Context, email string) (domain.User, error) {
	collection := ur.database.Collection(ur.collection)
	filter := bson.M{"email": email}
	var user domain.User
	err := collection.FindOne(c, filter).Decode(&user)
	return user, err
}

func (ur *userRepository) GetByID(c context.Context, id string) (domain.User, error) {
	collection := ur.database.Collection(ur.collection)

	var user domain.User

	idHex, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return user, err
	}

	err = collection.FindOne(c, bson.M{"_id": idHex}).Decode(&user)
	return user, err
}

func (ur *userRepository) SetNewPassword(c context.Context, user domain.User, newpassword string) error {
	collection := ur.database.Collection(ur.collection)
	filter := bson.M{"_id": user.ID}
	update := bson.M{"$set": bson.M{"password": newpassword}}

	_, err := collection.UpdateOne(c, filter, update)

	return err
}

func (ur *userRepository) SetUserOtp(c context.Context, user domain.User, otp string) error {
	collection := ur.database.Collection(ur.collection)
	filter := bson.M{"_id": user.ID}
	update := bson.M{"$set": bson.M{"otp": otp}}

	_, err := collection.UpdateOne(c, filter, update)

	return err
}
